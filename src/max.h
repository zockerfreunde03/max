#ifndef MAX_H
#define MAX_H

#define PTS_TO_WIN 1000000
#define PTS_MAX 1000
#define CHECK_MAX(x, y) (x == 2 && y == 1) || (x == 1 && y == 2)
#define PAIR(x) 100 * x
#define _NORMAL(x, y) 10 * x + y
#define NORMAL(x, y) _NORMAL(MAX(x, y), MIN(x, y))
#define MIN(x, y) x < y ? x : y
#define MAX(x, y) x > y ? x : y

struct game_stats {
	int pts;
	int casts;
	int max;
};

struct game_stats *init(void);
void cast(struct game_stats *);
int game_over(struct game_stats *);
float calc_percentage(int, int);
int die(void);
void delete(struct game_stats *);
void reset(struct game_stats *);

#endif

#include <stdlib.h>
#include <sys/random.h>
#include "max.h"

struct game_stats *init()
{
	struct game_stats *m = calloc(1, sizeof(struct game_stats));

	if (!m)
		return NULL;

	m->pts = 0;
	m->casts = 0;
	m->max = 0;
	return m;
}

void cast(struct game_stats *m)
{
	int d1 = random() % 6 + 1;
	int d2 = random() % 6 + 1;

	if (d1 == d2) {
		m->pts += PAIR(d1);
	} else if (CHECK_MAX(d1, d2)) {
		m->max += 1;
		m->pts += PTS_MAX;
	} else {
		m->pts += NORMAL(d1, d2);
	}

	m->casts += 1;
}

int game_over(struct game_stats *m)
{
	return m->pts >= PTS_TO_WIN;
}

float calc_percentage(int max, int casts)
{
	return (max * 100.0) / casts;
}

void delete(struct game_stats *m)
{
	reset(m);
	free(m);
}

void reset(struct game_stats *m)
{
	m->pts = 0;
	m->casts = 0;
	m->max = 0;
}

#include <sys/random.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "max.h"

#define _XOPEN_SOURCE 600

int main(int argc, char *argv[])
{
	char *s = calloc(513, sizeof(char));

	if (!s)
		return EXIT_FAILURE;

	if (getrandom(s, 512, GRND_NONBLOCK) == -1) {
		free(s);
		return EXIT_FAILURE;
	}

	srandom(*(unsigned int *)s);
	free(s);

	char *spacer = "-------------------------";
	int max = 0;
	int points = 0;
	int casts = 0;
	int repeat = 0;
	int repeat_cnt = 0;
	if (argc == 2) {
		char *tmp;
		repeat = 1;
		repeat_cnt = strtol(argv[1], &tmp, 0);
		if (argv[1] == tmp || errno != 0)
			return EXIT_FAILURE;
	}

	struct game_stats *m;
	m = init();
max_start:
	while (!game_over(m))
		cast(m);
	printf("Casts: %d\nPoints: %d\nMax: %d (%.3f%%)\n", m->casts, m->pts,
	       m->max, calc_percentage(m->max, m->casts));

	if (repeat) {
		points += m->pts;
		max += m->max;
		casts += m->casts;
	}

	if (repeat_cnt > 0) {
		reset(m);
		--repeat_cnt;
		printf("%s\n", spacer);
		goto max_start;
	}
	delete(m);

	if (repeat)
		printf("%s\nTotal Points: %d\nTotal Max: %d (%.3f%%)\n", spacer,
		       points, max, calc_percentage(max, casts));

	return EXIT_SUCCESS;
}

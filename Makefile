CC=gcc
CFLAGS=-Wall -O2 -pipe \
       -march=native -O2 \
       -D_FORTIFY_SOURCE=2 \
       -fstack-protector-strong \
       -fcf-protection -fpie \
       -fPIC -pedantic -pedantic-errors \
       -fno-delete-null-pointer-checks \
       -D_DEFAULT_SOURCE \
       -Wextra

OBJQ=max.o main.o

%.o: src/%.c
	$(CC) -c -o $@ $< $(CFLAGS)

max: $(OBJQ)
	$(CC) -o $@ $^ $(CFLAGS)

clean:
	rm -rf max *.o
